# Aide et ressources de OPUS Bruker pour Synchrotron SOLEIL

[<img src="https://www.bruker.com/content/dam/bruker/int/en/products-and-solution/infrared-and-raman/product-images/opus/OPUS_banner.png.transform.0/herolarge/image.OPUS_banner.png" width="150"/>](https://www.bruker.com/en/products-and-solutions/infrared-and-raman/opus-spectroscopy-software.html)

## Résumé

- transformation de Fourier, contrôle optique, traitement mathématiques (moyenne, normalisation... )
- Privé

## Sources

- Code source:  https://www.bruker.com/en/products-and-solutions/infrared-and-raman/opus-spectroscopy-software/downloads.html
- Documentation officielle:  http://shaker.umh.es/investigacion/OPUS_script/OPUS_5_BasePackage.pdf

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien),  Difficile (très technique),  entre les deux

## Format de données

- en entrée: binaire
- en sortie: opus,  xpn
- sur la Ruche
